package com.bitreactive.others;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

@WebServlet("/checkconn")
public class CheckConn extends HttpServlet {

	private static final long serialVersionUID = -1552976610575086860L;
	private static final int timeout = 5000; //5 seconds

	public CheckConn() {
		super();
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
		String port = req.getParameter("p");
		String context = req.getParameter("c");
		
		String ip = req.getRemoteAddr();
		final HttpParams httpParams = new BasicHttpParams();
	    HttpConnectionParams.setConnectionTimeout(httpParams, timeout);
	    HttpClient client = new DefaultHttpClient(httpParams);
	    HttpGet httpget = null;
	    
	    if (port == null) {
	    	if (context == null) {
	    		httpget = new HttpGet("http://" + ip);
	    	} else {
	    		httpget = new HttpGet("http://" + ip + "/" + context + "/");
	    	}
	    } else {
	    	if (context == null) {
	    		httpget = new HttpGet("http://" + ip + ":" + port);
	    	} else {
	    		httpget = new HttpGet("http://" + ip + ":" + port + "/" + context + "/");
	    	}
	    }
	    HttpResponse response;
		try {
			response = client.execute(httpget);
			int statusCode = response.getStatusLine().getStatusCode();
		    if (statusCode == HttpStatus.SC_OK) {
				resp.getWriter().write("OK");
		    } else if (statusCode == HttpStatus.SC_REQUEST_TIMEOUT) {
		    	resp.getWriter().write("TIMEOUT");
		    } else {
				resp.getWriter().write("NOK");
		    }
		} catch (Exception e) {
			e.printStackTrace();
			try {
				resp.getWriter().write("NOK");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
}
